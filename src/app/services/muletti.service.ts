import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Muletto } from '../interfaces/airplane';


@Injectable({
  providedIn: 'root'
})
export class MulettiService {
  constructor(
    private http: HttpClient
  ) { }

  /**
   * Creazione di un muletto.
   */
  createMuletto(payload: Muletto): Observable<Muletto> {
    return this.http.post<Muletto>('http://host.com/path/muletti', payload)
  }

  deleteMuletto() { }

  patchMuletto() {

  }

  getMuletto() { }

}
