import { Injectable, OnInit } from '@angular/core';
import { Product } from '../interfaces/products-interfaces';
import { BehaviorSubject } from 'rxjs';
import { ProductCardService } from 'src/app/services/product-card.service';

@Injectable({ providedIn: 'root' })
export class CartService {

    cart$ = new BehaviorSubject<Product[]>([]);

    constructor(
        private pcs: ProductCardService
    ) {
        this.pcs.productToShow$.subscribe((newProduct: Product | null) => {
            if (newProduct) {
                const oldCart = this.cart$.getValue();
                let newCart: Product[];
                if (oldCart.includes(newProduct)) {
                    newCart = oldCart.filter(p => p.id !== newProduct.id);
                } else {
                    newCart = oldCart.concat([newProduct]);
                }
                this.cart$.next(newCart);
            } else {
                // non fare niente
            }
        });
    }

}