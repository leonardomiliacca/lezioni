import { Injectable, SkipSelf } from '@angular/core';
import { Observable, of } from 'rxjs';
import { USERS } from '../constants/user';
import { HttpClient } from '@angular/common/http';
import { delay, map } from 'rxjs/operators';
import { User } from '../interfaces/user';

@Injectable({
    providedIn: 'root'
})
export class UsersService {

    loginToken!: string | null;

    constructor(
        private http: HttpClient
    ) {
        const storedToken = localStorage.getItem('loginToken');
        if (storedToken) {
            this.loginToken = storedToken;
        } else {
            this.loginToken = null;
        }
    }

    setLoginToken(newToken: string): void {
        this.loginToken = newToken;
        localStorage.setItem('loginToken', newToken);
    }

    login(username: string, password: string): Observable<any> {

        return this.http.post(`https://dummyjson.com/auth/login`, {
            username: 'kminchelle',
            password: '0lelplR'
        });
    }

    getUserByToken(token: string): Observable<any> {
        // return of(USERS[14]);
        return this.http.get<User>(`https://dummyjson.com/users/15`)
            .pipe(
                delay(500)
            );
    }

    getUsers(): Observable<any> {
        return of([]);
    }

}