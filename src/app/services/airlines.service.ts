import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class AirlinesService {
    constructor(
        private http: HttpClient
    ) { }

    getAirlines(): Observable<any> {
        return this.http.get('https://dummyjson.com/products');
    }

}