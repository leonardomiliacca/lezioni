import { Injectable } from '@angular/core';
import { Observable, map, catchError, throwError, of, delay } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ProductsResponse } from '../interfaces/products-interfaces';
import { UsersService } from './users.service';

@Injectable({ providedIn: 'root' })
export class ProductsService {

    constructor(
        private http: HttpClient,
        private us: UsersService
    ) { }

    /**
     * 
     * recupera tutte le categorie
     */
    getCategories(): Observable<string[]> {
        return this.http.get<string[]>(`https://dummyjson.com/products/categories`);
    }

    /**
     * Recupera tutti i prodotti in base alla categoria
     * @param category la categoria
     * @returns 
     */
    getProductsForCategory(category: string): Observable<ProductsResponse> {
        return this.http.get<ProductsResponse>(`https://dummyjson.com/products/category/${category}`)
            .pipe(
                delay(500)
            );
    }

    getProducts(): Observable<ProductsResponse> {
        return this.http.get<ProductsResponse>('https://dummyjson.com/products', {
            // headers: {
            //     Authorization: `Bearer ${this.us.loginToken}`,
            // }
        })
            .pipe(
                delay(300),
                catchError(err => {
                    console.log('ERRORE INTERCETTATO', err);
                    return of();
                })

            );
    }

}