import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommonAPIService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Creazione di un oggetto.
   */
  creaOggetto<T>(payload: T, path: 'aerei' | 'muletti'): Observable<T> {
    return this.http.post<T>(`${environment.host}/path/${path}`, payload);
  }



}
