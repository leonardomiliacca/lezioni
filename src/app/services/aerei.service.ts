import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Aereo } from '../interfaces/airplane';

@Injectable({
  providedIn: 'root'
})
export class AereiService {



  constructor(
    private http: HttpClient
  ) { }

  /**
   * Creazione di un aeroplano
   */
  createAirplane(payload: Aereo): Observable<Aereo> {
    return this.http.post<Aereo>('http://host.com/path/aerei', payload)
  }

  deleteAirplane() { }

  patchAirplane() {

  }

  getAirlines(): Observable<any> {
    return this.http.get<any>(`https://api.instantwebtools.net/v1/airlines`);
  }

  getAirplane(id: number): Observable<Aereo> {
    return this.http.get<Aereo>(`http://host.com/path/aerei/${id}`)
  }

}
