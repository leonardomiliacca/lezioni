import { Injectable } from '@angular/core';
import { Product } from '../interfaces/products-interfaces';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ProductCardService {

    public productToShow$ = new BehaviorSubject<Product | null>(null);

    constructor() { }

    setProduct(product: Product | null): void {
        this.productToShow$.next(product);
    }

}
