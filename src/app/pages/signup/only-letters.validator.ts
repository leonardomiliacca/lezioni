import { AbstractControl, ValidationErrors, ValidatorFn, FormControl } from '@angular/forms';

export function onlyLettersValidator(letters: RegExp): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        const ctrl: FormControl = control as FormControl;
        // const {value} = ctrl;
        // const value = ctrl.value;
        console.warn('funzione validazione eseguita');
        const value = ctrl.value as string;
        const match = value.match(letters);

        if (!match) {
            return { containsNonLetters: true }
        } else {
            return null;
        }

        // return !match ? { containsNonLetters: true } : null;
    };
}