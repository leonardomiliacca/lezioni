import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { onlyLettersValidator } from './only-letters.validator';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  form!: FormGroup;

  ONLY_REGEXP = new RegExp(/^[a-z]{1,1000}$/);

  constructor(
    private fb: FormBuilder
  ) {
    this.form = this.fb.group({
      username: ['', [Validators.required, onlyLettersValidator(this.ONLY_REGEXP)]],
      email: [null, [Validators.required, Validators.email]],
      anagrafica: this.fb.group({
        eta: [null],

        nome: [null],
        cognome: [null],
        cf: [null]
      }),
      indirizzo: this.fb.group({
        via: [null],
        citta: [null],
        cap: [null]
      })
    });
  }

  ngOnInit(): void {
    console.log('FORM GROU', this.form);
  }

  get usernameCtrl(): FormControl {
    return this.form.get('username') as FormControl;
  }

  get emailCtrl(): FormControl {
    return this.form.get('email') as FormControl;
  }


}
