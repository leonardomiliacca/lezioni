import { NgModule } from '@angular/core';

import { DashboardComponent } from './dashboard.component';
import { RouterModule } from '@angular/router';
import { CardModule } from 'src/app/card/card.module';
import { CommonModule } from '@angular/common';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilterByDestinationPipe } from './filter-by-destination.pipe';
import { TimePipe } from './time.pipe';
import { HoverDirectiveModule } from '../../directives/hover-directive.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { EcommerceInterceptor } from 'src/app/interceptors/e-commerce.interceptor';
import { UsersService } from '../../services/users.service';
import { SidebarModule } from 'primeng/sidebar';
import { ButtonModule } from 'primeng/button';
import { ProductCardService } from 'src/app/services/product-card.service';
import { CartService } from '../../services/cart.service';
import { MustHaveAnOrderGuard } from 'src/app/guards/must-have-an-order.guard';
import { MustAcceptClausesGuard } from 'src/app/guards/must-accept-clauses.guard';
import { RedirectToClausesGuard } from 'src/app/guards/redirect-to-clauses.guard';

@NgModule({
  imports: [
    CardModule,
    HoverDirectiveModule,
    InputTextModule,
    SidebarModule,
    ButtonModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{
      path: '',
      component: DashboardComponent,
      // canActivate: [RedirectToClausesGuard],
      children: [
        {
          path: 'profile',
          loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule),
          // canActivate: [RedirectToClausesGuard]
        },
        {
          path: 'cart',
          loadChildren: () => import('./cart/cart.module').then(m => m.CartModule),
          canLoad: [MustHaveAnOrderGuard],
          canActivate: [RedirectToClausesGuard]
        },
        {
          path: 'products',
          loadChildren: () => import('./products/products.module').then(m => m.ProductsModule),
          canActivate: [RedirectToClausesGuard]
        },
        {
          path: 'accept-clauses',
          loadChildren: () => import('./accetta-clausole/accetta-clausole.module').then(m => m.AccettaClausoleModule)
        },
        {
          path: 'profile-resolved',
          loadChildren: () => import('./profile-resolved/profile-resolved.module').then(m => m.ProfileResolvedModule)
        },
        {
          path: 'user-input',
          loadChildren: () => import('./user-input/user-input.module').then(m => m.UserInputModule)
        },
        {
          path: 'users',
          loadChildren: () => import('./users/users.module').then(m => m.UsersModule)
        }
      ]
    }])
  ],
  exports: [],
  declarations: [
    DashboardComponent,
    FilterByDestinationPipe,
    TimePipe
  ],
  providers: [
    UsersService,
    // MessageService,
    CartService,
    RedirectToClausesGuard,
    MustHaveAnOrderGuard,
    ProductCardService,
    MustAcceptClausesGuard,
    { provide: HTTP_INTERCEPTORS, useClass: EcommerceInterceptor, multi: true }
  ],
})
export class DashboardModule { }
