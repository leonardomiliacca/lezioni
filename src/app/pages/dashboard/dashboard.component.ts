import { Component, OnInit } from '@angular/core';
import { MOCKED_FLIGHTS } from 'src/app/mocks/flights';
import { HttpClient } from '@angular/common/http';
import { ProductsService } from 'src/app/services/products.service';
import { ProductsResponse, Product } from '../../interfaces/products-interfaces';
import { concatMap, debounceTime, distinctUntilChanged, exhaustMap, forkJoin, map, mergeMap, Observable, of, shareReplay, switchMap } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProductCardService } from '../../services/product-card.service';
import { CartService } from '../../services/cart.service';
import { MessageService } from 'primeng/api';

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

  showMainCard = true;

  MOCKED_FLIGHTS = MOCKED_FLIGHTS;

  filterByProductName = '';

  products: Product[] = [];

  products$!: Observable<Product[]>;

  highestPrice!: number;

  filterForm!: FormGroup;

  showSidebar = false;


  constructor(
    private http: HttpClient,
    private ps: ProductsService,
    private fb: FormBuilder,
    public pcs: ProductCardService,
    public cs: CartService,
    private ms: MessageService
  ) {
    this.filterForm = this.fb.group({
      product: [null]
    });
  }

  ngOnInit() {

    this.products$ = this.ps.getProducts().pipe(
      map((value: ProductsResponse) => {
        return value.products;
      }),
      shareReplay()
    );

    this.pcs.productToShow$.subscribe((product: Product | null) => {
    })

    // this.filterForm
    //   .valueChanges
    //   .pipe(

    //     concatMap((value: { product: string }) => {
    //       return this.ps.getProductsForCategory(value.product);
    //     })
    //   )
    //   .subscribe();

    // this.ps.getCategories()
    //   .pipe(
    //     concatMap((categories: string[]) => {
    //       const calls = categories.map((category: string) => {
    //         return this.ps.getProductsForCategory(category);
    //       });

    //       return forkJoin(calls);
    //     })
    //   )
    //   .subscribe(console.log);

    // this.products$.subscribe({
    //   next: (response: Product[]) => {
    //     console.log('chiamata eseguita', response);
    //     this.products = response;
    //     const prices: number[] = this.products.map((product) => {
    //       return product.price
    //     });
    //     this.highestPrice = Math.max(...prices);
    //   },
    //   error: (err) => {
    //     console.warn('ERRORE', err)
    //   },
    //   complete: () => {
    //     console.log('completato')
    //   }
    // });
  }
}
