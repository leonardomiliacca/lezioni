import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import { RouterModule } from '@angular/router';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';


@NgModule({
  declarations: [
    UsersComponent
  ],
  imports: [
    ButtonModule,
    DialogModule,
    TableModule,
    CommonModule,
    RouterModule.forChild([{
      path: '',
      component: UsersComponent
    }])
  ]
})
export class UsersModule { }
