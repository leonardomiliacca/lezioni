import { Component, OnInit } from '@angular/core';
import { USERS } from '../../../constants/user';
import { ProductsService } from '../../../services/products.service';
import { Observable } from 'rxjs';
import { Product } from 'src/app/interfaces/products-interfaces';
import { ProductsResponse } from '../../../interfaces/products-interfaces';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  // users = USERS;

  showModal = false;

  products$!: Observable<ProductsResponse>;

  selectedProduct!: Product;

  constructor(
    private ps: ProductsService
  ) { }

  ngOnInit(): void {
    this.products$ = this.ps.getProducts();
  }

  openModalAndSetSelectedProduct(product: Product): void {
    this.showModal = true;
    this.selectedProduct = product;
  }

}
