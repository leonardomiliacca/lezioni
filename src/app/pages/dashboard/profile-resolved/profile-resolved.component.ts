import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { of, Subscription } from 'rxjs';
import { User } from '../../../interfaces/user';
import { USERS } from '../../../constants/user';

@Component({
  selector: 'app-profile-resolved',
  templateUrl: './profile-resolved.component.html',
  styleUrls: ['./profile-resolved.component.scss']
})
export class ProfileResolvedComponent implements OnInit, OnDestroy {

  user!: User;

  sub!: Subscription;

  constructor(
    private ar: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.user = this.ar.snapshot.data['prova'];
    console.log("l'utente risolto è", this.user);

    this.sub = of(USERS).subscribe();
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
