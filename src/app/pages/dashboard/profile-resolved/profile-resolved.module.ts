import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileResolvedComponent } from './profile-resolved.component';
import { RouterModule } from '@angular/router';
import { UserResolver } from '../../../resolvers/user.resolver';



@NgModule({
  declarations: [
    ProfileResolvedComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '',
      component: ProfileResolvedComponent,
      resolve: { prova: UserResolver }
    }])
  ]
})
export class ProfileResolvedModule { }
