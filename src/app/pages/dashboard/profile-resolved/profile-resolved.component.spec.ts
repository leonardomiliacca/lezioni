import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileResolvedComponent } from './profile-resolved.component';

describe('ProfileResolvedComponent', () => {
  let component: ProfileResolvedComponent;
  let fixture: ComponentFixture<ProfileResolvedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileResolvedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileResolvedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
