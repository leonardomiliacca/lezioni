import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterByDestination'
})

export class FilterByDestinationPipe implements PipeTransform {
    transform(flights: any[], filter: { departure: string, destination: string }): any[] {
        console.log({
            flights,
            filter
        });
        return flights;
    }
}