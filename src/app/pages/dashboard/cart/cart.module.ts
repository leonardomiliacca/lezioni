import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart.component';
import { RouterModule } from '@angular/router';
import { MustHaveAnOrderGuard } from '../../../guards/must-have-an-order.guard';
import { MustHaveAnOrderToShowGuard } from 'src/app/guards/must-have-an-order-to-show.guard';



@NgModule({
  declarations: [
    CartComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '',
      component: CartComponent,
      // canActivate: [MustHaveAnOrderToShowGuard],

    }])
  ],
  providers: [
    MustHaveAnOrderToShowGuard
  ]
})
export class CartModule { }
