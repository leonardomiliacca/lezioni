import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { RouterModule } from '@angular/router';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { EcommerceInterceptor } from 'src/app/interceptors/e-commerce.interceptor';
import { UsersService } from '../../../services/users.service';
import { ProductsService } from '../../../services/products.service';
import { UserResolver } from 'src/app/resolvers/user.resolver';

@NgModule({
  declarations: [
    ProfileComponent
  ],
  imports: [
    CardModule,
    ButtonModule,

    CommonModule,
    RouterModule.forChild([{
      path: '',
      component: ProfileComponent,
      // resolve: { user: UserResolver }
    }])
  ],
  providers: [
    ProductsService,
  ]
})
export class ProfileModule { }
