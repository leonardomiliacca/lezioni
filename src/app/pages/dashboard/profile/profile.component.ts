import { Component, OnDestroy, OnInit } from '@angular/core';
import { UsersService } from '../../../services/users.service';
import { Observable, of, Subscription } from 'rxjs';
import { USERS } from '../../../constants/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {

  user$!: Observable<any>;

  sub!: Subscription;

  constructor(
    private us: UsersService
  ) { }

  ngOnInit(): void {
    this.user$ = this.us.getUserByToken(this.us.loginToken as string);
    this.sub = of(USERS).subscribe();

  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
