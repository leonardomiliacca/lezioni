import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccettaClausoleComponent } from './accetta-clausole.component';
import { RouterModule } from '@angular/router';

import { MustAcceptClausesGuard } from '../../../guards/must-accept-clauses.guard';

@NgModule({
  declarations: [
    AccettaClausoleComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([{
      path: '',
      component: AccettaClausoleComponent,
      canDeactivate: [MustAcceptClausesGuard]
    }])
  ]
})
export class AccettaClausoleModule { }
