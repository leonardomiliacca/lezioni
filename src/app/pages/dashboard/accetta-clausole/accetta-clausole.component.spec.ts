import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccettaClausoleComponent } from './accetta-clausole.component';

describe('AccettaClausoleComponent', () => {
  let component: AccettaClausoleComponent;
  let fixture: ComponentFixture<AccettaClausoleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccettaClausoleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccettaClausoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
