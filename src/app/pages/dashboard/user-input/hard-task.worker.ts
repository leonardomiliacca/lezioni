/// <reference lib="webworker" />

addEventListener('message', ({ data }) => {

  // console.log(data);

  const { variabiliUtente, formula } = data;

  const executionContext: any = {};
  variabiliUtente.map((variabile: { label: string, value: number }) => {
    executionContext[variabile.label] = variabile.value;
  });

  const tokens: string[] = formula
    .split(' ').filter(Boolean); // eccolo l'assassino

  console.log(tokens);
  const resolvedFormula = tokens.reduce((previous: string, token: string) => {
    if (token === '+' || token === '-') {
      return previous + `${token}`;
    } else {
      return previous + ` this.${token} `;
    }
  }, 'return ')

  console.log({ executionContext, resolvedFormula });

  const fn = new Function(resolvedFormula).bind(executionContext);
  console.log(fn());
  postMessage(null);
});
