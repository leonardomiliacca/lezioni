import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';

type UserVariable = {
  label: string;
  value: number;
};

@Component({
  selector: 'app-user-input',
  templateUrl: './user-input.component.html',
  styleUrls: ['./user-input.component.scss']
})
export class UserInputComponent implements OnInit {

  form!: FormGroup;

  outcome = 0;

  constructor(
    private fb: FormBuilder
  ) {
    this.form = this.fb.group({
      nuovaVariabile: [null, [Validators.required]],
      variabiliUtente: this.fb.array([
        this.fb.group({
          label: 'a',
          value: 5
        }),
        this.fb.group({
          label: 'b',
          value: 9
        })
      ]),
      formula: [null]
    });
  }

  ngOnInit(): void {



    this.form.valueChanges.subscribe((formValue) => {
      const worker = new Worker(new URL('./hard-task.worker', import.meta.url));


      worker.onmessage = ({ data }) => {
        console.log(`page got message: ${data}`);
      };
      worker.postMessage(formValue);
    });

  }

  addVariable(): void {
    const { value } = this.form;
    console.log('value', value);
    this.variabiliUtente.push(
      this.fb.group({
        label: value.nuovaVariabile,
        value: [null]
      })
    );
    this.form.get('nuovaVariabile')?.reset();
  }

  get variabiliUtente(): FormArray {
    return this.form.get('variabiliUtente') as FormArray;
  }

}
