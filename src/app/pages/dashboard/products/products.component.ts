import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../../services/products.service';
import { Observable } from 'rxjs';
import { ProductsResponse } from 'src/app/interfaces/products-interfaces';
import { ProductCardService } from '../../../services/product-card.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products$!: Observable<ProductsResponse>;

  constructor(
    private ps: ProductsService,
  ) { }

  ngOnInit(): void {
    this.products$ = this.ps.getProducts();
  }

}
