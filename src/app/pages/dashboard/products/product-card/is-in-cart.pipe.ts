import { Pipe, PipeTransform } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { Product } from '../../../../interfaces/products-interfaces';

@Pipe({
    name: 'isInCart'
})
export class IsInCartPipe implements PipeTransform {


    transform(cart: Product[] | null, product: Product): boolean {

        if (cart) {
            const productInCard: Product | undefined = cart.find(p => {
                return p.id === product.id;
            });

            return !!productInCard;

        } else {
            return false;
        }

        // return productInCard ? true : false;

        // if (productInCard) {
        //     return true;
        // } else {
        //     return false;
        // }

        // console.assert(currentCart.includes(product));
        // return currentCart.includes(product);
    }
}