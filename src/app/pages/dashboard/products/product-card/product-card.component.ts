import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/interfaces/products-interfaces';
import { ProductCardService } from 'src/app/services/product-card.service';
import { CartService } from '../../../../services/cart.service';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss'],
  providers: [
    // ProductCardService
  ]
})
export class ProductCardComponent implements OnInit {

  @Input() product!: Product;

  // @Input() productIsIncluded: boolean = false;

  // same = false;

  constructor(
    private pcs: ProductCardService,
    public cs: CartService
  ) { }

  ngOnInit(): void {
    // this.pcs.productToShow$.subscribe((newProduct: Product | null) => {
    //   console.log(`VALORE RICEVUTO DA ${this.product.title}`, newProduct?.title);
    //   if (newProduct) {
    //     if (this.product.id === newProduct.id) {
    //       this.same = true;
    //     } else {
    //       this.same = false;
    //     }
    //   } else {
    //     this.same = false;
    //   }
    // });

  }

  selectProduct(product: Product): void {
    const currentCart: Product[] = this.cs.cart$.getValue();
    const newCart = currentCart.concat([product]);
    this.cs.cart$.next(newCart);
  }

  deselectProduct(product: Product): void {
    const currentCart: Product[] = this.cs.cart$.getValue();
    const newCart = currentCart.filter((p: Product) => {
      return p.id !== product.id;
    });
    this.cs.cart$.next(newCart);
  }

}
