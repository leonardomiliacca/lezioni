import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductCardComponent } from './product-card.component';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { ProductCardService } from 'src/app/services/product-card.service';
import { IsInCartPipe } from './is-in-cart.pipe';

@NgModule({
  declarations: [
    ProductCardComponent,
    IsInCartPipe
  ],
  imports: [
    CommonModule,
    ButtonModule,
    CardModule
  ],
  exports: [
    ProductCardComponent
  ],
  providers: [
    // ProductCardService
  ]
})
export class ProductCardModule { }
