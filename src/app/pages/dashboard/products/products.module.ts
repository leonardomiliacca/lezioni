import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products.component';
import { RouterModule } from '@angular/router';
import { ProductCardModule } from './product-card/product-card.module';
import { ProductCardService } from 'src/app/services/product-card.service';


@NgModule({
  declarations: [
    ProductsComponent
  ],
  imports: [
    CommonModule,
    ProductCardModule,
    RouterModule.forChild([{
      path: '',
      component: ProductsComponent
    }])
  ],
  providers: [
    // ProductCardService
  ]
})
export class ProductsModule { }
