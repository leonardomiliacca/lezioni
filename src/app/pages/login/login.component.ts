import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'login',
  templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {

  loginForm!: FormGroup;



  numeroACaso = 4;
  stringaQualunque = 'ciao';

  constructor(
    private us: UsersService,
    private r: Router,
    private ms: MessageService
  ) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      username: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required])
    });
    console.log(this.us.loginToken);
  }

  /**
   * Effettua il login chiamando un server remoto e gestendo la risposta.
   */
  login(): void {

    const { value } = this.loginForm;

    this.us.login(value.usename, value.password)
      .subscribe({
        next: (response) => {
          console.log(response);
          this.ms.add({
            detail: 'Login effettuato con successo',
            summary: 'Operazione riuscita',
            severity: 'success'
          });
          this.us.setLoginToken(response.token);
          this.r.navigate([`/dashboard/profile`]);
        }
      });


  }

  valoreCambiatoHandler($event: string): void {
    console.log('il valore è cambiato', $event);
  }


  get usernameCtrl(): FormControl {
    return this.loginForm.get('username') as FormControl;
  }
}
