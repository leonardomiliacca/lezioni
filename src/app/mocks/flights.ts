export const MOCKED_FLIGHTS = [{
    partenza: 'Roma',
    arrivo: 'Francoforte',
    data: new Date(),
    durata: 2.5
}, {
    partenza: 'Roma',
    arrivo: 'Madrid',
    data: new Date(),
    durata: 3.5
}, {
    partenza: 'Roma',
    arrivo: 'Berlino',
    data: new Date(),
    durata: 3
}, {
    partenza: 'Roma',
    arrivo: 'Oslo',
    data: new Date(),
    durata: 6
}];