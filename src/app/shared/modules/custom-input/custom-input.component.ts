import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'custom-input',
  templateUrl: 'custom-input.component.html'
})

export class CustomInputComponent implements OnInit {

  @Input() numero!: number;

  @Output() valoreCambiato = new EventEmitter<string>();

  constructor() { }

  ngOnInit() { }

  emitNewValue($event: any): void {
    console.group('emitNewValue');
    console.log('numero è', this.numero);
    console.log('valore del tag input è', $event.target.value);
    console.groupEnd();
    this.valoreCambiato.emit($event.target.value);
  }
}
