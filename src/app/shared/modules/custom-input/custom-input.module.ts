import { NgModule } from '@angular/core';

import { CustomInputComponent } from './custom-input.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    CustomInputComponent
  ],
  declarations: [
    CustomInputComponent
  ]
})
export class CustomInputModule { }
