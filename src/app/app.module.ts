import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InputTextModule } from 'primeng/inputtext';
import { HoverDirective } from './directives/hover.directive'
import { UsersService } from './services/users.service';
import { ToastModule } from 'primeng/toast';

// import { SomatolineInterceptor } from './interceptors/e-commerce.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EcommerceInterceptor } from './interceptors/e-commerce.interceptor';
import { ProductCardService } from './services/product-card.service';
import { MessageService } from 'primeng/api';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    ToastModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    InputTextModule
  ],
  exports: [
  ],
  providers: [
    MessageService,
    // UsersService,
    // ProductCardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
