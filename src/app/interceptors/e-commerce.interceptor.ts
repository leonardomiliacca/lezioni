import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of, BehaviorSubject } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UsersService } from '../services/users.service';

@Injectable()
export class EcommerceInterceptor implements HttpInterceptor {
    constructor(
        private router: Router,
        private us: UsersService
    ) {
    }

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {

        const token = this.us.loginToken as string;

        console.log({
            request,
            next,
            token
        });

        if (token) {
            request = this.addHeaders(request, token);
        }

        return next.handle(request)
            .pipe(
                catchError((err: HttpErrorResponse) => {
                    if (err.status === 401) {
                        console.log(request.headers);
                        return throwError(() => err);
                    } else {
                        if (!request.headers.has('skipmessage')) {

                        }
                    }
                    // devo restituire uno stream, pertanto devo wrappare
                    // l'errore in un throwError
                    return throwError(() => err);
                }) as any
            );

    }


    private addHeaders(
        originalReq: HttpRequest<any>,
        token: string
    ): HttpRequest<any> {
        const somatolineHeaders: any = {
            Authorization: token, // resta sempre questo
        };
        const req = originalReq.clone({
            setHeaders: somatolineHeaders
        });
        return req;
    }

}
