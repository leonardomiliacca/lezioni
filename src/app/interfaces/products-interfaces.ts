export interface ProductsResponse {
    limit: number; // 30
    products: Product[];
    skip: number; // 0
    total: number; // 100 
}

export interface Product {
    brand: string; // "Apple"
    category: string; //"smartphones"
    description: string; //"An apple mobile which is nothing like apple"
    discountPercentage: number; // 12.96
    id: number; // 1
    images: string[]; // ["https://dummyjson.com/image/i/products/1/1.jpg", "https://dummyjson.com/image/i/products/1/2.jpg",…]
    price: number; // 549
    rating: number; // 4.69
    stock: number; // 94
    thumbnail: string; //"https://dummyjson.com/image/i/products/1/thumbnail.jpg"
    title: string; //"iPhone 9"
}