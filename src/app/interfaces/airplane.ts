export enum Priorita {
  URGENTE = 1,
  BASSA = 0
}

export interface Veicolo {
  id: number;
}

export interface Aereo extends Veicolo {
  codice: string;
  operativo: boolean;
  // passeggeri: any[];
}

export interface Muletto extends Veicolo {
  codice?: string;
  ruote: number;
}

export interface RispostaServer {
  muletti: Muletto[];
  aerei: Aereo[];
  operatori: any[];
}

const serverResponse: Record<'muletti' | 'operatori', number> = {
  muletti: 4,
  operatori: 8
}

