type UserPermissions = 'mustAcceptClauses' | 'clausesAccepted';

export interface User {
    createdAt?: Date | string;
    token: string; // e674bb09e95439,
    id: number; // 1,
    firstName: string; // Terry,
    lastName: string; // Medhurst,
    maidenName: string; // Smitham,
    age: 50,
    gender: 'male' | 'female',
    email: string; // atuny0@sohu.com,
    phone: string; // +63 791 675 8914,
    username: string; // atuny0,
    password: string; // 9uQFF1Lh,
    birthDate: string; // 2000-12-25,
    image: string; // https://robohash.org/hicveldicta.png?size=50x50&set=set1,
    bloodGroup: string; // A−,
    height: 189,
    weight: 75.4,
    permissions?: UserPermissions,
    eyeColor: 'Green' | 'Blue' | 'Black' | 'Brown'; // Green,
    hair:
    {
        color: 'Black' | 'Blonde' | 'Brown' | 'Red' | 'Blue' | 'Green' | 'White' | 'Gray'; // Black,
        type: string; // Strands
    },
    domain: string; // slashdot.org,
    ip: string; // 117.29.86.254,
    address: {
        address: string; // 1745 T Street Southeast,
        city: string; // Washington,
        coordinates: {
            lat: number; // 38.867033,
            lng: number; // -76.979235
        },
        postalCode: string; // 20020,
        state: string; // DC
    },
    macAddress: string; // 13:69:BA:56:A3:74,
    university: string; //  Capitol University,
    bank:
    {
        cardExpire: string; // 06/22,
        cardNumber: string; // 50380955204220685,
        cardType: string; // maestro,
        currency: string; // Peso,
        iban: string; // NO17 0695 2754 967
    },
    company: {
        address: {
            address: string; // 629 Debbie Drive,
            city: string; // Nashville,
            coordinates: {
                lat: number; // 36.208114,
                lng: number; // -86.58621199999999
            },
            postalCode: string; // 37076,
            state: string; // TN
        },
        department: string; // Marketing,
        name: string; // Blanda-O'Keefe,
        title: string; // Help Desk Operator
    },
    ein: string; // 20-9487066,
    ssn: string; // 661-64-2976,
    userAgent: string; // Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/12.0.702.0 Safari/534.24
}