import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { UsersService } from '../services/users.service';
import { map, Observable } from 'rxjs';
import { User } from '../interfaces/user';

@Injectable()
export class RedirectToClausesGuard implements CanActivate {
    constructor(
        private r: Router,
        private us: UsersService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UrlTree | boolean> {
        console.group('RedirectToClausesGuard');
        console.log('inizio controllo guardia');
        return this.us.getUserByToken(this.us.loginToken as string)
            .pipe(
                map((user: User) => {
                    console.log('l\'utente finale è', user);
                    console.groupEnd();
                    if (user.permissions === 'mustAcceptClauses') {
                        return this.r.createUrlTree(['/dashboard/accept-clauses']);
                    } else {
                        return true;
                    }
                })
            )

        // return this.r.createUrlTree(['/dashboard/clauses']);
    }
}