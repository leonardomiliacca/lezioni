import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { UsersService } from '../services/users.service';
import { AccettaClausoleComponent } from '../pages/dashboard/accetta-clausole/accetta-clausole.component';
import { map, Observable } from 'rxjs';
import { User } from '../interfaces/user';

@Injectable()
export class MustAcceptClausesGuard implements CanDeactivate<AccettaClausoleComponent> {
    constructor(
        private us: UsersService,
        private r: Router
    ) { }

    canDeactivate(
        component: AccettaClausoleComponent,
        currentRoute: ActivatedRouteSnapshot,
        currentState: RouterStateSnapshot,
        nextState: RouterStateSnapshot
    ): Observable<boolean> {
        console.group('MustAcceptClausesGuard');
        return this.us.getUserByToken(this.us.loginToken as string)
            .pipe(
                map((user: User) => {
                    if (user.permissions === 'mustAcceptClauses') {
                        console.log('l\'utente rimane sulla pagina')
                        console.groupEnd();
                        return false;
                    } else {
                        console.log('l\'utente se ne può');
                        console.groupEnd();
                        return true;
                    }
                })
            )
            ;
    }
}