import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { UsersService } from '../services/users.service';
import { User } from '../interfaces/user';

@Injectable({
    providedIn: 'root'
})
export class UserResolver implements Resolve<Observable<User>> {

    constructor(
        private us: UsersService
    ) {

    }

    resolve(): Observable<User> {
        return this.us.getUserByToken(this.us.loginToken as string);
    }
}