import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() showFooter = true;

  @Output() dismissBtn = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

}
