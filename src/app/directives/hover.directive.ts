import { Directive, ElementRef, HostBinding, HostListener, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[appHover]'
})
export class HoverDirective implements OnInit {

  @HostListener('mouseover') onMouseEnter() {
    this.highlight(true);
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlight(false);
  }

  @HostBinding('attr.title') currentTitle = 'titolone';

  constructor(private el: ElementRef) { }

  ngOnInit(): void {
    this.el.nativeElement.style.transition = 'filter .3s ease, transform .3s ease';
  }

  private highlight(highlight: boolean) {
    if (highlight) {
      this.currentTitle = 'no titolo'
    } else {
      this.currentTitle = 'sì titolo'
    }
    this.el.nativeElement.style.filter = highlight ? 'brightness(1.4)' : 'brightness(1.0)';
    this.el.nativeElement.style.transform = highlight ? 'scale(1.1)' : 'scale(1.0)';
  }


}
