import { Component, OnInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { Aereo, Priorita, Muletto } from './interfaces/airplane';
import { AereiService } from './services/aerei.service';
import { CommonAPIService } from './services/common-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title!: string;

  airlines$!: Observable<any>;

  constructor(
    private as: AereiService
  ) {

  }

  ngOnInit(): void {
  }


}
